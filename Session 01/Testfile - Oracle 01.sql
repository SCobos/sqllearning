-- Execute this in sqldeveloper application

SELECT table_name FROM user_tables;


SELECT employee_id, first_name, last_name, department_id FROM employees
WHERE department_id = 100
ORDER BY first_name asc;


SELECT * FROM departments WHERE location_id = 1700;


SELECT employee_id, first_name, last_name FROM employees
WHERE first_name = 'Kevin';


SELECT employee_id, first_name, last_name FROM employees
WHERE first_name = 'Steven'
AND last_name = 'King';


SELECT employee_id, first_name, last_name, salary FROM employees
WHERE salary > 6000
ORDER BY salary asc;


SELECT employee_id, first_name, last_name, hire_date FROM employees
WHERE hire_date > '01-JAN-07'
ORDER BY hire_date asc;


SELECT employee_id, first_name, last_name, hire_date FROM employees
WHERE hire_date BETWEEN '01-JAN-07' AND '01-JAN-09'
ORDER BY hire_date asc;


SELECT first_name, last_name FROM employees
WHERE last_name BETWEEN 'Chung' AND 'Markle';


SELECT first_name, last_name FROM employees
WHERE first_name BETWEEN 'Alberto' AND 'Kevin'
ORDER BY first_name desc;


SELECT first_name, last_name FROM employees
WHERE first_name BETWEEN 'B%' AND 'D%'
ORDER BY first_name asc;


SELECT first_name, last_name, manager_id FROM employees
WHERE manager_id IN (101, 103, 123)
ORDER BY manager_id asc;


SELECT first_name, last_name FROM employees
WHERE last_name LIKE 'A%';


SELECT first_name, last_name FROM employees
WHERE last_name NOT IN ('Chung' , 'Ozer')
ORDER BY last_name asc;


SELECT first_name, last_name FROM employees
WHERE last_name LIKE '%mit%';


SELECT employee_id, last_name, hire_date FROM employees
WHERE hire_date LIKE '%08'
ORDER BY hire_date;


SELECT employee_id, last_name, commission_pct FROM employees
WHERE commission_pct IS NULL;


SELECT employee_id, last_name, salary FROM employees
WHERE salary > 10000
OR last_name LIKE 'S%'
ORDER BY salary asc;



SELECT job_id, JOB_TITLE FROM jobs;

SELECT first_name, last_name, job_id, salary FROM employees
WHERE (job_id = 'IT_PROG'
OR job_id = 'SA_MAN')
AND salary > 5000;


-- Sustituion variable
SELECT employee_id, first_name, last_name, salary FROM employees
WHERE employee_id = '&eid';


define eid = 115
SELECT employee_id, first_name, last_name, department_id
FROM employees
WHERE employee_id = &eid;


SELECT job_id, job_title FROM jobs;
SELECT employee_id, first_name, last_name, department_id
FROM employees
WHERE job_id = '&jtitle';


SELECT job_id, LOWER(job_id), INITCAP(job_id),
job_title, UPPER(job_title)
FROM jobs;

describe dual;

SELECT user FROM dual;

SELECT sysdate from dual;

SELECT LENGTH('Hello world!!!!!!!!') AS length FROM dual;

SELECT job_title,
SUBSTR(job_title, 1, 5),
SUBSTR(job_title, 8)
FROM jobs;

SELECT job_title,
INSTR(job_title, 'Stock') AS Test
FROM jobs
ORDER BY Test DESC;

SELECT first_name, last_name FROM employees;

SELECT CONCAT(first_name, last_name)
FROM employees;

SELECT last_name || ', ' || first_name
FROM employees;

SELECT last_name ||' '|| first_name
FROM employees;

SELECT 314.43235,
ROUND(314.43235, 2),
ROUND(314.43235, 0),
TRUNC(314.43235)
FROM dual;

SELECT employee_id, first_name, last_name, salary AS Yearly,
ROUND((salary / 12), 2) AS Monthly
FROM employees
ORDER By salary;

SELECT TO_CHAR(1210.73, '$9,999.00')
FROM dual;

SELECT TO_DATE('2010/04/30', 'yyyy/mm/dd')
FROM dual;


SELECT * FROM employees;

SELECT AVG(salary) FROM employees;
SELECT MIN(salary) FROM employees;
SELECT MAX(salary)FROM employees;
SELECT SUM(salary) FROM employees;
SELECT COUNT(*) FROM employees;